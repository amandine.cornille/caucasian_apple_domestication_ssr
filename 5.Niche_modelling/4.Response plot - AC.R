library(biomod2)

setwd("/Users/amandinecornille/Google\ Drive/stagiaires_colleagues/Hamid_Hamed_Iran/analyses/MaxENT/")

source("./fonctions/response.plot_modified.R", local = TRUE)

sp.list <- read.csv("./data_cours/species_list2.csv", sep = ";")
baseline <- readRDS("./Data/baseline.rds")

initial.wd <- getwd()
setwd("Models")
for (i in 1:nrow(sp.list)) 
{
  sp <- as.character(sp.list$sp[i])
  load(paste0(sp, "/model.runs"))
  load(paste0(sp, "/run.data"))
  
  cur.vars <- model.runs@expl.var.names
  
  models.to.plot <- BIOMOD_LoadModels(model.runs)
  
  resp <- response.plot3(models = models.to.plot,
                         Data = baseline[[cur.vars]],
                         fixed.var.metric = "sp.mean",
                         show.variables = cur.vars,
                         run.data = run.data)


 #resp <- response.plot3(models = c("orientalis_PA1_RUN1_GLM", "orientalis_PA2_RUN1_GLM", "orientalis_PA1_RUN2_GLM", "orientalis_PA2_RUN2_GLM"),
  #                    Data = baseline[[cur.vars]],
  #                     fixed.var.metric = "sp.mean",
    #                   show.variables = cur.vars,
     #                  run.data = run.data,
       #                main= "Malus orientalis GLM")
  
  
  resp.df <- cbind(melt(resp[, "Var", , ]),
                   melt(resp[, "Pred", , ])$value)
  colnames(resp.df) <- c("Index", "Variable", "Model", "Var.value", "Response")


  
  # 
  resp.df$Var.value[which(resp.df$Variable %in% paste("bio", 1:19, sep = ""))] <- 
    resp.df$Var.value[which(resp.df$Variable %in% paste("bio", 1:19, sep = ""))] / 10
  resp.df$Var.value[which(resp.df$Variable %in% paste("bio", 1:19, sep = ""))] <- 
    resp.df$Var.value[which(resp.df$Variable %in% paste("bio", 1:19, sep = ""))] / 100
  

  #
  setwd(initial.wd)
  write.table(resp.df, "./Models/orientalis_resp_curv.df.txt", quote=F, sep="\t")
  
  resp.df$Variable <- factor(resp.df$Variable, levels = cur.vars)

 #####AC
 ggplot(resp.df, aes(x = Var.value, y = Response)) + stat_smooth(method = "gam", formula = y ~ s(x, bs = "cs")) +
   facet_wrap(~Variable, scales = "free_x") + theme_bw() + ylim(0, 1.1) + xlab("Variable value ??") +
    ylab("Probability of occurence") + main="Malus orientalis GLM"

  p <-ggplot(resp.df, aes(x = Var.value, y = Response))+  stat_smooth(method = "gam")   + 
    facet_wrap(~Variable, scales = "free_x") + theme_bw() + ylim(0, 1.1) + xlab("Variable value") + ylab("Probability of occurence")

  png(paste0(initial.wd, "response_plot_", sp, ".png"), width = 550 * 4.2, height = 550 * 4.2, res = 300)
  
  print(p)
  

}
setwd(initial.wd)

