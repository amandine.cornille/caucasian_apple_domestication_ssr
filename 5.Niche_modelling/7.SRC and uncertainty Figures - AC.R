# ----------- Species Range Change -----------
library(raster)

setwd("/Users/amandinecornille/Google\ Drive/stagiaires_colleagues/Hamid_Hamed_Iran/analyses/MaxENT/")


sp.list <- read.csv("./data_cours2/species_list2.csv", sep = ";")


pdf("./Graphiques/src.pdf")
for (sp in as.character(sp.list$sp))
{
  src <- stack(paste("Outputs/src_", sp, ".grd", sep = ""))
  for(j in names(src))
  {
    plot(src[[j]], legend = F, 
         main = paste0(sp, " range change\n", j), 
         col = c("#FF4100", "#3016B0", "#F2F2F2FF", "#2DD700"), xpd = NA, las = 1)
    legend("topleft", cex=0.75,
           fill = c("#F2F2F2FF", "#FF4100", "#3016B0", "#2DD700"), 
           legend = c("Unsuitable", "Lost", "Kept", "New"), ncol = 4, xpd = NA)
  }
}
dev.off()



pdf("./Graphiques/cont.pdf")
for(sp in as.character(sp.list$sp))
{
probs <- stack(paste("./Outputs/em_cont_", sp, ".grd", sep = ""))
plot(probs, main = paste(sp, names(probs)))
}
dev.off()

pdf("./Graphiques/binary.pdf")
for(sp in as.character(sp.list$sp))
{
  binary <- stack(paste("./Outputs/em_bin_", sp, ".grd", sep = ""))
  plot(binary, main = paste(sp, names(binary)))
}
dev.off()


initial.wd <- getwd()


# ---------- Figure standard deviations -----------
sp <- "orientalis"
sd.sp <- stack(paste0(initial.wd, "/Outputs/em_sd_", sp, ".grd"))
em.sp <- stack(paste0(initial.wd, "/Outputs/em_cont_", sp, ".grd"))

cv.sp <- sd.sp/em.sp


cool = rainbow(50, start=rgb2hsv(col2rgb('yellow'))[1], end=rgb2hsv(col2rgb('blue'))[1])
warm = rainbow(50, start=rgb2hsv(col2rgb('red'))[1], end=rgb2hsv(col2rgb('yellow'))[1])
lut  = c(rev(cool), rev(warm))

plot(cv.sp[["baseline"]], col = lut)
plot(sd.sp[["baseline"]], col = lut)




