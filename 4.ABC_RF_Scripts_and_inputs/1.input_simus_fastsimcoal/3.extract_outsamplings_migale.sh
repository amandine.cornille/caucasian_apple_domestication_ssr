#Amandine Cornille
#10 August 2020
#Launch ABC in parallel

#MERGE ALL THE RESULTS FOR ALL PRIOR SET AND COPY THEM IN THE NEW FOLDER all_results

#copy all results files no GF results in the res_folder

#USE AS ./3extract_outsamplings_ABC_HAMID_migale SCENARIOID

SCENARIO_name=$1
FOLDER_NAME=$(pwd)
rm -rf res
mkdir res

#j scenarios number
for j in {41..65} 
do
	cd sc$j'_'$@
	head -n 1 sc$j'_'$@'_1'/out_sampling1.txt > res_sc$j'_'$@'.txt'
	#i is the number of parallelize folder per scenario
	for i in {1..20}
	do 
	tail -n +2 sc$j'_'$@'_'$i/out_sampling1.txt >> res_sc$j'_'$@'.txt'
	done
	cp res_sc$j'_'$@'.txt' $FOLDER_NAME/res
	cd ..
done


