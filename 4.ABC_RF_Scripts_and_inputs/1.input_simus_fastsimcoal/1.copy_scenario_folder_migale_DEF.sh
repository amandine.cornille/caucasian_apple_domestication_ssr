#Amandine Cornille
#10 August 2020
#Launch ABC in parallel


#COPY X time a source folder to launch in parallel job on Migale

#Use as ./1.copy_scenario_folder_migale_DEF.sh SCENARIO_ID 
#e.g. ./1.copy_scenario_folder_migale_DEF.sh fullGF

scenario_name=$1

for j in {41..65} 
do
	cd sc$j'_'$@
	mkdir sc$j'_'$@'_1'
	mv *.* sc$j'_'$@'_1'
	mv ABCtoolbox arlsumstat fastsimcoal21 sc$j'_'$@'_1/'
	for i in {2..20} 
	do cp -r sc$j'_'$@'_1' sc$j'_'$@'_'$i 
	done
	cd ..
done

