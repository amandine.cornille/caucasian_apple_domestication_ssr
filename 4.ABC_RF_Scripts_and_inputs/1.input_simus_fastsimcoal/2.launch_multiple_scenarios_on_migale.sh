#Amandine Cornille
#10 August 2020
#Launch ABC in parallel

#USE AS ./2.launch_multiple_scenarios_on_migale.sh SCENARIOID
 #USE AS ./2.launch_multiple_scenarios_on_migale.sh fullGF

SCENARIO_name=$1

#j is number of scenarios GF
for j in {41..65} 
do
	cd sc$j'_'$@
	#i is the number of parallelize folder per scenario
		for i in {1..20}
		do 
			cd sc$j'_'$@'_'$i
			qsub launch_migale_ABC.sh
			cd ..
		done
	cd ..
done
