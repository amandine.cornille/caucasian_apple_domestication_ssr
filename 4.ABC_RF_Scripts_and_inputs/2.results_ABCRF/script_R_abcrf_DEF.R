#Amandine Cornille
#10 August 2020
#ABC RF script

rm(list=ls())
#command arg
args=commandArgs(T)
n_first_line=as.numeric(args[1])
#n_first_line=10000
n_first_line=1

setwd("")


#install.packages("abcrf",repos="http://cran.univ-lyon1.fr/")
library(abcrf)
library(dplyr)
library(tidyverse)
library(rlist)
library(Rcpp)

##############################################
# Importation and definition of the datasets #
##############################################

###################
#OBSERVED DATA####
##################
#observe stats
statobs <- read.table("5groups_cw.obs",header = TRUE)

#nsumstat
nsumstat<-ncol(statobs)

#####################
#SIMULATED SS ######
#####################
#put all the ss_files in the same folder such as the results folder is only ".txt" ss files 

files <- dir(pattern = "*.txt")
nb_scenarios<-length(files)
file_ss_extension<-sub('\\.txt$', '', files) 

######read all the ss files
my.data <- lapply(files, 
                  read.table,
                  header=TRUE, 
                  sep="\t", 
                  dec=".",
                  stringsAsFactors=F,
                  colClasses="numeric")

####give a name to each dataframe in the list of scenarios
names(my.data)<-file_ss_extension
names_list<-names(my.data)

#### ONLY KEEP 8000 LINES PER SCENARIO

n_first_line=1
Nsimu_per_model=8000

my.data<-lapply(my.data, function(x){
    x[c(n_first_line:(Nsimu_per_model)),]
  })


#### CREATE INDEPENDANT DATAFRAME FROM THE LIST OF dataframe
for (i in 1:length(names_list)) {
  assign(names_list[i], as.data.frame(my.data[[i]]))
}

#bind all the SS
ss<-bind_rows(my.data)
nrow(ss)
head(ss)

#Keep only the ss from the obs data

head(ss)

#Prepare the index for each scenario

list_of_index <- list()
#index<-lapply()
for (i in 1:length(names_list)){
  vecname<-paste('index_', names_list[i], sep = '')
  list_of_index[[i]]<-assign(vecname, as.vector(c(rep(paste(names_list[i]), (Nsimu_per_model)))))
  #list_of_index[[i]]<-get(vecname)
}

#create the index
index<-combine(list_of_index)
index<-as.factor(index)

#reference table
ref_table<-cbind(index, ss)
ncol_refTable<-ncol(ref_table)
head(ref_table)

################################
#different GROUPING#############
################################

################################
#GENE FLOW ALL#############
################################

#### purple TRUE WILD 
from_sievKAZT<-c("ss_sc1_GF05")

from_domT<-c("ss_sc2_GF05", "ss_sc3_GF05", "ss_sc4_GF05", "ss_sc12_GF05")


cropIR_from_ORIRT=c("ss_sc5_GF05",
                   "ss_sc6_GF05",
                   "ss_sc7_GF05")
                   #"ss_sc11_GF05")
                   
cropIR_from_ORIRT_sis=c("ss_sc11_GF05")

cropIR_from_ORARMT=c("ss_sc8_GF05",
                    "ss_sc9_GF05",
                    "ss_sc10_GF05")

CropIRgreenfromDOM_CropIRpurplefromORIRT<-c("ss_sc20_GF05",
                                                 "ss_sc14_GF05",
                                                 "ss_sc15_GF05")


CropIRgreenfromDOM_CropIRpurplefromORART<-c("ss_sc17_GF05",
                                           "ss_sc18_GF05",
                                           "ss_sc19_GF05")

CropIRpurplefromDOM_CropIRgreenfromORIRT<-c("ss_sc21_GF05",
                                           "ss_sc22_GF05",
                                           "ss_sc23_GF05")


CropIRpurplefromDOM_CropIRgreenfromORART<-c("ss_sc25_GF05",
                                           "ss_sc26_GF05",
                                           "ss_sc27_GF05")


#### purple wild feral 

from_sievKAZT_PiC<-c("ss_sc41_GF05")

from_domT_PiC<-c("ss_sc42_GF05", "ss_sc43_GF05", "ss_sc44_GF05")

cropIR_from_ORIRT_PiC=c("ss_sc45_GF05",
                        "ss_sc46_GF05",
                        "ss_sc47_GF05")

cropIR_from_ORARMT_PiC=c("ss_sc48_GF05",
                         "ss_sc49_GF05",
                         "ss_sc50_GF05")


CropIRgreenfromDOM_CropIRpurplefromORIR_PiC<-c("ss_sc52_GF05",
                                               "ss_sc53_GF05",
                                               "ss_sc54_GF05")

CropIRgreenfromDOM_CropIRpurplefromORAR_PiC<-c("ss_sc56_GF05",
                                               "ss_sc57_GF05",
                                               "ss_sc58_GF05")

CropIRpurplefromDOM_CropIRgreenfromORIR_PiC<-c("ss_sc59_GF05",
                                               "ss_sc60_GF05",
                                               "ss_sc61_GF05")


CropIRpurplefromDOM_CropIRgreenfromORAR_PiC<-c("ss_sc63_GF05",
                                               "ss_sc64_GF05",
                                               "ss_sc65_GF05")

###############################
#############GF ONLY###########
###############################

##### 1. evolutionary history of the purple

grouping_scenarios=list(
  c(from_sievKAZT,from_sievKAZT_PiC,from_domT, from_domT_PiC, 
  cropIR_from_ORIRT, cropIR_from_ORIRT_sis,cropIR_from_ORIRT_PiC,
  cropIR_from_ORARMT, cropIR_from_ORARMT_PiC),
  c(CropIRgreenfromDOM_CropIRpurplefromORIRT,CropIRgreenfromDOM_CropIRpurplefromORIR_PiC,
  CropIRgreenfromDOM_CropIRpurplefromORART,CropIRgreenfromDOM_CropIRpurplefromORAR_PiC,
  CropIRpurplefromDOM_CropIRgreenfromORIRT,CropIRpurplefromDOM_CropIRgreenfromORIR_PiC,
  CropIRpurplefromDOM_CropIRgreenfromORART,CropIRpurplefromDOM_CropIRgreenfromORAR_PiC)
)


##### 2. single versus multiple origins
grouping_scenarios=list(
  c(from_sievKAZT_PiC, from_domT_PiC, cropIR_from_ORIRT_PiC, cropIR_from_ORARMT_PiC), 
  c(CropIRgreenfromDOM_CropIRpurplefromORIR_PiC,
    CropIRgreenfromDOM_CropIRpurplefromORAR_PiC, 
    CropIRpurplefromDOM_CropIRgreenfromORIR_PiC,
    CropIRpurplefromDOM_CropIRgreenfromORAR_PiC))


##### 3. or DOM
grouping_scenarios=list(
  c(from_sievKAZT_PiC, from_domT_PiC),
  c(cropIR_from_ORIRT_PiC,cropIR_from_ORARMT_PiC)
)

##### or ORAIR or OR ARM

grouping_scenarios=list(
  c(cropIR_from_ORIRT_PiC),
  c(cropIR_from_ORARMT_PiC)
)


##################################################################################
# PLOT THE SUMMARY STATISTICS AND THE OBSERVED DATA AS AN HISTOGRAM###############
##################################################################################
library(reshape2)
library(ggplot2)
d_obs <- melt(statobs)
d <- melt(ss)

 pdf("Hist_free.pdf", width = 10, height = 10)
 ggplot(d,aes(x = value)) + 
  facet_wrap(~variable,scales = "free") + 
  geom_histogram() + 
  geom_vline(data = d_obs, aes(xintercept = value))
dev.off()


##################################################################################
# Contributions to the random forests and LDA projections of the reference table #
##################################################################################
par(mfrow=c(1, 1))
# Two graphics/figures providing:
# (i) the contributions of the 30 most important statistics to the RF (file named graph_varImpPlot.pdf).
# (ii) LDA projections of the reference table for the different scenarios plus the observed dataset (cf. black star in the figure; file named graph_lda.pdf).
# e.g. Fig. S6 and Fig. S7 in Pudlo et al. 2016.
mc.rf <- abcrf(formula=index~., data=ref_table,ntree=100, group = grouping_scenarios ) # Computation related to the forest classification.
mc.rf <- abcrf(formula=index~., data=ref_table,ntree=100) # Computation related to the forest classification.
#pdf(file.pdf,width=10,height=10,paper='special')
plot(mc.rf, ref_table, obs=statobs[1,], pdf=TRUE)
#dev.off()


#######################################################
# Definition of the key parameters of the RF analysis #
#######################################################

ntrees_in_forest=500 	# Number of trees in the random forest (default=500; Pudlo et al. 2016).
nrep=10	# Number of replicates analyses used to calculated the average and the standard deviation (sd) of posterior probability (default=10, Fraimout et al. 2017). 
ncores_on_speed=20
nscenarios=nb_scenarios	# Number of compared scenarios (models) in the reference table.
#nscenarios=6	# Number of compared scenarios (models) in the reference table.
nSS=as.numeric(ncol(statobs))	# Size of the reference table (i.e. number of simulated datasets) that will be used to do the RF analysis.
#Nref=nscenarios*10000 # nb de simus + #
Nref=nscenarios*Nsimu_per_model # nb de simus + #

##############################
# Random forest computations #
##############################

TotOut=list()		# Create a new empty list (necessary for the next loop).
TotOut=list()		# Create a new empty list (necessary for the next loop).
TotVote=list()		# Create a new empty list (necessary for the next loop).
TotBest=list()		# Create a new empty list (necessary for the next loop).
TotProb=list()		# Create a new empty list (necessary for the next loop).
T1 <- Sys.time()	# T1: starting time of the analysis (including reading the file reftable.txt).
for (i in 1:nrep) {
  #mc.rf <- abcrf(formula=index~., data=ref_table, paral=TRUE, ntree=ntrees_in_forest, ncores=ncores_on_speed) # Computation related to the forest classification.
  mc.rf <- abcrf(formula=index~., data=ref_table, paral=TRUE, group = grouping_scenarios,  ntree=ntrees_in_forest, ncores=ncores_on_speed) # Computation related to the forest classification.
  Out <- capture.output(mc.rf) 		# Send the console output (i.e. numerical results for the global prior error rates and the matrix of confusion (i.e. prior error rates detailed for each scenario) to a object "Out". Estimation from 10,000 pseudo-observed datasets from the reference table.
  TotOut[[i]] <- Out		# Add the numerical results for the global prior error rates and the matrix of confusion to a result list.
  Best <- predict(object=mc.rf, obs=statobs, training=ref_table, paral=TRUE, ntree=ntrees_in_forest)
  TotVote[[i]] <- Best$vote		# Add the pproportion of vote per scenario to a result list.
  TotBest[[i]] <- Best$allocation	# Add the best scenario to a result list.
  TotProb[[i]] <- Best$post.prob	# Add the posterior probability of the best scenario to a result list.
  T2 <- Sys.time()			# T2: ending time of the analysis.
  duration = difftime(T2, T1)		# Duration of the analysis (i.e. T2-T1).
  print(duration)				# 1.176981 hours
}

mc.rf$model.rf$predictions
#devtools::install_github("imbs-hl/ranger", force=TRUE)

##################
# Number of vote #
##################

TOTVOTE <- do.call(rbind.data.frame, TotVote) # Convert the list of best scenario into a data frame (useful for the exportation).
write.csv(TOTVOTE, "PropVotes.csv") # Export results into a csv file.
NBVOTE <- TOTVOTE*ntrees_in_forest     # Calculate the number of vote per scenario by multiplying the proportion of vote to 500 (i.e. the total number of vote).
for (i in 1:nscenarios) { # Calculate the mean number of vote per scenario and the corresponding standard deviation (sd).
  NBVOTE[nrep+1, i] <- mean(as.numeric(NBVOTE[1:nrep, i]))
  NBVOTE[nrep+2, i] <- sd(as.numeric(NBVOTE[1:nrep, i]))
}
NBVOTE$Analysis <- c(1:nrep, "Mean", "sd") # Add a new column indicating (for each line) the corresponding analysis (e.g. "Mean"...).
write.csv(NBVOTE, "NbVotes.csv") # Export results into a csv file.
#NBVOTE <- NBVOTE[c("Analysis", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10")] # Reorder the column (i.e. 1st: "Analysis"; 2nd: "Prior_Error_Rate").
write.csv(NBVOTE, "NbVotes2.csv") # Export results into a csv file.


#################
# Best scenario #
#################

TOTBEST <- do.call(rbind.data.frame, TotBest) ; colnames(TOTBEST) <- "Best_Scen" # Convert the list of best scenario into a data frame (useful for the exportation).
write.csv(TOTBEST, "BestModel.csv") # Export results into a csv file.
#TOTBEST_SD <- sd(TOTBEST[, 1]) # Calculate the standard deviation (sd) of the best scenario, which = 0 here if this is the same best scenario for each replicates analyses.
#if (TOTBEST_SD==0) {           # Define the best scenario (i.e. the same best scenario for each replicates analyses) according to the standard deviation (sd) value.
#  BEST <- as.numeric(TOTBEST[1, 1])
#}
#if (TOTBEST_SD>=0) {# Define the best scenario (i.e. the same best scenario for each replicates analyses) according to the standard deviation (sd) value.
#  BEST <- as.numeric(TOTBEST[1, 1])
#}



######################################################################################
# Mean and standard deviation (sd) of the posterior probability of the best scenario #
######################################################################################

TOTPROB <- do.call(rbind.data.frame, TotProb) ; colnames(TOTPROB) <- "Post_Prob" # Convert the list of Post Prob into a data frame (useful for the exportation).
MEAN <- mean(TOTPROB[1:nrep, 1]) ; TOTPROB[nrep+1, 1]=MEAN # Computes the averaged posterior probability and add it to the data frame.
SD <- sd(TOTPROB[1:nrep, 1]) ; TOTPROB[nrep+2, 1]=SD       # Computes the standard deviation (sd) of the posterior probability and add it to the data frame.
TOTPROB$Analysis <- c(1:nrep, "Mean", "sd")    # Add a new column indicating (for each line) the corresponding analysis (e.g. "Mean"...).
TOTPROB <- TOTPROB[c("Analysis", "Post_Prob")] # Reorder the column (i.e. 1st: "Analysis"; 2nd: "Post_Prob").
write.csv(TOTPROB, "PostProb.csv")             # Export results into a csv file.


############################################################
# Mean and standard deviation (sd) of the prior error rate #
############################################################

TOTOUT_A <- do.call(rbind.data.frame, TotOut) # Convert the list of results into a data frame (useful for the exportation).
TOTOUT_B <- TOTOUT_A[, 7]                     # Export the column containing the prior error rate (i.e. the 7th column).
TOTOUT_C <- gsub(x=TOTOUT_B, pattern="Out-of-bag prior error rate: ", replacement=""); TOTOUT_C <- gsub(x=TOTOUT_C, pattern="%", replacement="") # Remove the text into the column (keep only the numerical values).
PRIOR_ERROR_RATE <- data.frame(as.numeric(TOTOUT_C)) ; colnames(PRIOR_ERROR_RATE) <- "Prior_Error_Rate" # Create a data frame containing the prior error rate for each analysis. 
PRIOR_ERROR_RATE_MEAN <- mean(PRIOR_ERROR_RATE[0:nrep, 1]) ; PRIOR_ERROR_RATE[nrep+1, 1]=PRIOR_ERROR_RATE_MEAN # Computes the averaged prior error rate and add it to the data frame.
PRIOR_ERROR_RATE_SD <- sd(PRIOR_ERROR_RATE[0:nrep, 1]) ; PRIOR_ERROR_RATE[nrep+2, 1]=PRIOR_ERROR_RATE_SD       # Computes the standard deviation (sd) of the prior error rate and add it to the data frame.
PRIOR_ERROR_RATE$Analysis <- c(1:nrep, "Mean", "sd") # Add a new column indicating (for each line) the corresponding analysis (e.g. "Mean"...).
PRIOR_ERROR_RATE <- PRIOR_ERROR_RATE[c("Analysis", "Prior_Error_Rate")] # Reorder the column (i.e. 1st: "Analysis"; 2nd: "Prior_Error_Rate").
write.csv(PRIOR_ERROR_RATE, "PriorErrorRate.csv") # Export results into a csv file.



###############################
# Combine all tables into one #
###############################

ALL <- NBVOTE                                             # Create a new dataframe based on the dataframe containing the number of vote per scenario.
ALL$Post_Prob <- TOTPROB$Post_Prob                        # Add a new column indicating the posterior probability.
ALL$Prior_Error_Rate <- PRIOR_ERROR_RATE$Prior_Error_Rate # Add a new column indicating the posterior probability.
#write.csv(ALL, paste("ABCRF_Results", grouping_name, ".csv"))                 # Export results into a csv file.
write.csv(ALL, paste("ABCRF_Results", ".csv"))                 # Export results into a csv file.
  

############################################
# Evolution of the ABC-RF prior error rate #
############################################

err.rf <- err.abcrf(object=mc.rf, training=ref_table) # Provide the prior error rates for forests with different number of trees (e.g. Fig. 3 in Pudlo et al. 2016).
plot(err.rf, type="o", pch=16, las=1, xlab="Number of trees", ylab="Prior error rate") # Graphic providing prior error rates for forests with different number of trees.
dev.copy2pdf(file="error_vs_ntree.pdf", height=18, width=18) # Another way to export the figure in pdf.


